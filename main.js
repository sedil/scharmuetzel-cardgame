const {Observer} = require('./Observer');

function game(){
    let game = new Observer();
    game.handoutCards();

    /* Fill the slots with cards */
    while(true){
        game.fillActorSlot(0);
        if(!game.slotsAreValid(0)){
            for(let s = 0; s < 5; s++){
                let cards = game.actors[0].accessSlot(s).getSlotcards();
                cards.forEach(card => game.actors[0].pushCard(card));
            }
            console.log(game.actors[0].getActorcards().length);
            for(let s = 0; s < 5; s++){
                console.log(game.actors[0].accessSlot(s).getSlotcards().length);
            }
            console.warn('All Slots must contain at least one card');
        } else {
            break;
        }
    }

    while(true){
        game.fillActorSlot(1);
        if(!game.slotsAreValid(1)){
            for(let s = 0; s < 5; s++){
                let cards = game.actors[1].accessSlot(s).getSlotcards();
                cards.forEach(card => game.actors[1].pushCard(card));
            }
            console.log(game.actors[1].getActorcards().length);
            for(let s = 0; s < 5; s++){
                console.log(game.actors[1].accessSlot(s).getSlotcards().length);
            }
            console.warn('All Slots must contain at least one card');
        } else {
            break;
        }
    }
    game.gameloop();
}

game();