const {Cardvalue} = require("./Cardstat");
const {Card} = require("./Card");

/**
 * Manages all gamecards into an array
 */
class Cardstack {

    constructor(){
        this.cardstack = Array();
        this.createCards();
    }

    /**
     * Creates all gamecards. Use only once in the beginning
     */
    createCards(){
        for(let v = 0; v < 4; v++){
            for(let n = 1; n <= 13; n++){
                this.cardstack.push(new Card(n));
            }
        }
    
        this.cardstack.push(new Card(Cardvalue.JOKER));
        this.cardstack.push(new Card(Cardvalue.JOKER));
        this.cardstack.push(new Card(Cardvalue.JOKER));
    }

    /**
     * returns a card at the index n
     * @param {int} index index
     * @returns gamecard
     */
    getCardAt(index){
        return this.cardstack.splice(index,1)[0];
    }

    /**
     * shuffles gamecards from the cardstack
     */
    shuffleCards(){
        for(let rounds = 0; rounds < 500; rounds++){
            let index = Number((Math.random() * this.cardstack.length - 1).toFixed(0));
            let card = this.getCardAt(index);
            this.cardstack.push(card);
        }
    }

    /**
     * returns the entire cardstack
     * @returns cardstack
     */
    getCardstack(){
        return this.cardstack;
    }

    /**
     * DEBUG
     */
    printCardstack(){
        this.cardstack.forEach(elem => console.log(elem));
    }
}

module.exports = { Cardstack : Cardstack};