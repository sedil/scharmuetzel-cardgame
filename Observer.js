const READLINE = require('readline-sync');
const {Actor} = require('./Actor');
const {Cardstack} = require('./Cardstack');

/**
 * This is the controller class of this game
 */
class Observer {

    constructor(){
        this.jetons = 200;
        this.playerpoints = 0;
        this.bankpoints = 0;
        this.player_bet_flag = -1;
        this.bank_bet_flag = -1;
        this.patts = 0;
        this.continueCampagne = false;
        this.actors = [new Actor(), new Actor()];
        this.cardstack = new Cardstack();
        this.cardstack.shuffleCards();
    }

    /**
     * hand out 10 cards to the actors
     */
    handoutCards(){
        for(let n = 0; n < 10; n++){
            this.actors[0].pushCard(this.cardstack.getCardstack().shift());
            this.actors[1].pushCard(this.cardstack.getCardstack().shift());
        }
    }

    /**
     * determines who starts the game
     * @returns true means player starts and false bank starts
     */
    determineStarter(){
        while(true){
            const CARD_ONE = this.cardstack.getCardstack().shift();
            const CARD_TWO = this.cardstack.getCardstack().shift();
            if(CARD_ONE.getValue() > CARD_TWO.getValue()){
                console.log(`Player ${CARD_ONE.getValue()} : ${CARD_TWO.getValue()} Bank`);
                console.log('=> Player starts the game');
                this.cardstack.getCardstack().push(CARD_ONE);
                this.cardstack.getCardstack().push(CARD_TWO);
                return true;
            } else if (CARD_ONE.getValue() < CARD_TWO.getValue()){
                console.log(`Player ${CARD_ONE.getValue()} : ${CARD_TWO.getValue()} Bank`);
                console.log('=> Bank starts the game');
                this.cardstack.getCardstack().push(CARD_ONE);
                this.cardstack.getCardstack().push(CARD_TWO);
                return false;
            } else {
                console.log(`Player ${CARD_ONE.getValue()} : ${CARD_TWO.getValue()} Bank`);
                this.cardstack.getCardstack().push(CARD_ONE);
                this.cardstack.getCardstack().push(CARD_TWO);
            }
        }
    }

    /**
     * helps to fill the slots with cards
     * @param {int} actorindex 0 = player, 1 = bank
     */
    fillActorSlot(actorindex){
        const ACTOR = this.actors[actorindex];

        while(ACTOR.getActorcards().length > 0){
            // list all available gamecards for this actor
            if(actorindex == 0){
                console.log(`Player : AVAILABLE CARDS`);
            } else {
                console.log(`Bank : AVAILABLE CARDS`);
            }

            for(let n = 0; n < ACTOR.actorcards.length; n++){
                console.log(`[${n}] : ${ACTOR.getActorcards()[n].getImage()}`);
            }
            console.log('');

            // list all slots
            for(let n = 0; n < 5; n++){
                console.log(`# SLOT ${n}`);
                console.log(ACTOR.accessSlot(n).printSlot_Console());
                console.log('');
            }

            let cardindex = Number(READLINE.question('Choose the Cardindex to insert a card into a slot : '));
            let slotindex = Number(READLINE.question('Choose the Slotindex : '));
            if(cardindex >= 0 && cardindex < ACTOR.getActorcards().length &&
            slotindex >= 0 && slotindex <= 4){

                if(ACTOR.accessSlot(slotindex).slot.length < 4){
                    ACTOR.accessSlot(slotindex).insertCardIntoSlot(ACTOR.takeCard(cardindex));

                    // just in case a card was inserted invalid
                    if(!this.slotcontentIsValid(actorindex, slotindex)){
                        ACTOR.pushCard(ACTOR.accessSlot(slotindex).getLastSlotcard());
                        continue;
                    }

                    // list updated stuff
                    console.log('');
                    console.log(`### UPDATE ###`);
                    console.log(`# AVAILABLE CARDS`);
                    for(let n = 0; n < ACTOR.actorcards.length; n++){
                        console.log(`[${n}] : ${ACTOR.getActorcards()[n].getImage()}`);
                    }
                    console.log('');
    
                    for(let n = 0; n < 5; n++){
                        console.log(`# SLOT ${n}`);
                        console.log(ACTOR.accessSlot(n).printSlot_Console());
                        console.log('');
                    }

                } else {
                    console.warn('[!] Choosed slot is full');
                    continue;
                }
            } else {
                console.warn('[!] Cardindex or Slotindex not valid');
                continue;
            }
        }
    }

    /**
     * the gameloop contains a loop with a maximum of 6 campagnes
     */
    gameloop(){
        console.log('GAME STARTS')
        let campagnecounter = 0;
        for(let n = 0; n < 6; n++){
            this.campagne();
            if(!this.continueCampagne){
                this.jetons = 0;
                console.log(`JETONS : ${this.jetons}`);
                break;
            } else {
                campagnecounter += 1;
                if(!this.continue()){
                    console.log(`JETONS : ${this.jetons}`);
                    break;
                }
                this.prepareNewCampagne();
                console.log(`Campagnes won : ${campagnecounter} / 6`);
                this.jetons *= 3;
                console.log(`JETONS : ${this.jetons}`);
            }
        }
    }

    /**
     * asks the player if he wants to continue with another campagne
     * @returns continue
     */
    continue(){
        const c = READLINE.question('Continue? (y/n)');
        if(c == 'y'){
            return true;
        } else if (c == 'n'){
            return false;
        } else {
            return false;
        }
    }

    /**
     * prepares a new campage if the player won the previous campagne
     */
    prepareNewCampagne(){
        let cardsFromSlots = Array();
        for(let n = 0; n < 5; n++){
            this.actors[0].accessSlot(n).getSlotcards().forEach(card => cardsFromSlots.push(card));
            this.actors[1].accessSlot(n).getSlotcards().forEach(card => cardsFromSlots.push(card));
            this.actors[0].accessSlot(n).setChoosedFlag(false);
            this.actors[1].accessSlot(n).setChoosedFlag(false);
        }
        cardsFromSlots.forEach(card => this.cardstack.getCardstack().push(card));
        console.log(this.cardstack.getCardstack().length);

        this.patts = 0;
        this.playerpoints = 0;
        this.bankpoints = 0;
        this.player_bet_flag = -1;
        this.bank_bet_flag = -1;
        this.cardstack.shuffleCards();
        this.handoutCards();

        // fill player's slots
        while(true){
            game.fillActorSlot(0);
            if(!game.slotsAreValid(0)){
                for(let s = 0; s < 5; s++){
                    let cards = game.actors[0].accessSlot(s).getSlotcards();
                    cards.forEach(card => game.actors[0].pushCard(card));
                }
                console.log(game.actors[0].getActorcards().length);
                for(let s = 0; s < 5; s++){
                    console.log(game.actors[0].accessSlot(s).getSlotcards().length);
                }
                console.warn('All Slots must contain at least one card');
            } else {
                break;
            }
        }
    
        // fill bank's slots
        while(true){
            game.fillActorSlot(1);
            if(!game.slotsAreValid(1)){
                for(let s = 0; s < 5; s++){
                    let cards = game.actors[1].accessSlot(s).getSlotcards();
                    cards.forEach(card => game.actors[1].pushCard(card));
                }
                console.log(game.actors[1].getActorcards().length);
                for(let s = 0; s < 5; s++){
                    console.log(game.actors[1].accessSlot(s).getSlotcards().length);
                }
                console.warn('All Slots must contain at least one card');
            } else {
                break;
            }
        }
    }

    /**
     * simulates a campagne. A campage contains a maxiumum of 5 rounds
     */
    campagne(){
        let player_starts = this.determineStarter();

        // List player slots
        for(let n = 0; n < 5; n++){
            this.actors[0].accessSlot(n).printSlot_Console();
        }

        console.log('');
        this.player_bet_flag = Number(READLINE.question('[PLAYER] Bet on winner slot : '));
        this.bank_bet_flag = Number(READLINE.question('[BANK] Bet on winner slot : '));
        console.log('');

        for(let n = 0; n < 5; n++){
            if(player_starts){

                console.log("Player's Turn");
                console.log('');

                const PLAYER_SLOTS = this.actors[0].slots;
                for(let n = 0; n < PLAYER_SLOTS.length; n++){
                    if(!PLAYER_SLOTS[n].IsChoosed()){
                        console.log(`PLAYER SLOT[${n}] ${PLAYER_SLOTS[n].fight()} | [ ${PLAYER_SLOTS[n].printSlot_Console()}]`);
                    }
                }

                console.log('');

                const BANK_SLOTS = this.actors[1].slots;
                for(let n = 0; n < BANK_SLOTS.length; n++){
                    if(!BANK_SLOTS[n].IsChoosed()){
                        console.log(`BANK SLOT[${n}] ${BANK_SLOTS[n].accessSlotcards().length}`);
                    }
                }
    
                console.log('');
                let player_slot = Number(READLINE.question('Player: Choose your slot:'));
                let bank_slot = Number(READLINE.question('Bank: Choose your slot:'));
                console.log('');

                this.compareSlots(player_starts, player_slot, bank_slot);
                if(this.showResults()){
                    break;
                }
                player_starts = !player_starts;
            } else {

                console.log("Bank's Turn");
                console.log('');

                const BANK_SLOTS = this.actors[1].slots;
                for(let n = 0; n < BANK_SLOTS.length; n++){
                    if(!BANK_SLOTS[n].IsChoosed()){
                        console.log(`BANK SLOT[${n}] : ${BANK_SLOTS[n].fight()} | [ ${BANK_SLOTS[n].printSlot_Console()}]`);
                    }
                }

                console.log('');

                const PLAYER_SLOTS = this.actors[0].slots;
                for(let n = 0; n < PLAYER_SLOTS.length; n++){
                    if(!PLAYER_SLOTS[n].IsChoosed()){
                        console.log(`PLAYER SLOT[${n}] ${PLAYER_SLOTS[n].accessSlotcards().length}`);
                    }
                }
    
                console.log('');
                let bank_slot = Number(READLINE.question('Bank: Choose your slot:'));
                let player_slot = Number(READLINE.question('Player: Choose your slot:'));
                console.log('');

                this.compareSlots(player_starts, bank_slot, player_slot);
                if(this.showResults()){
                    break;
                }
                player_starts = !player_starts;
            }  
        }
    }

    /**
     * compares the points of two slots
     * @param {boolean} isPlayer is player 
     * @param {int} n active actors slotindex
     * @param {int} m passive actors slotindex  
     */
    compareSlots(isPlayer,n,m){

        if(isPlayer){
            var active_slot = this.actors[0].accessSlot(n);
            var bank_slot = this.actors[1].accessSlot(m);

            active_slot.setChoosedFlag(true);
            bank_slot.setChoosedFlag(true);

            // Joker Rule

            const ACTIVE_JOKER_FLAG = this.examineSpecialcard_Joker(active_slot);
            if(ACTIVE_JOKER_FLAG){
                console.log('JOKER: SWAP SLOTS');
                let swapslot = this.swapSlots(active_slot,bank_slot);
                active_slot = swapslot[0];
                bank_slot = swapslot[1];
            }

            // Just in case the bank had also a joker before, swap back
            const BANK_JOKER_FLAG = this.examineSpecialcard_Joker(active_slot);
            if(BANK_JOKER_FLAG){
                console.log('JOKER: SWAP SLOTS');
                let swapslot = this.swapSlots(active_slot,bank_slot);
                active_slot = swapslot[0];
                bank_slot = swapslot[1];
            }

            // ### King and As Rule ### //

            const ACTIVE_AS_FLAG = this.examineSpecialcard_As(active_slot);
            const ACTIVE_KING_FLAG = this.examineSpecialcard_King(active_slot);
            const BANK_AS_FLAG = this.examineSpecialcard_As(bank_slot);
            const BANK_KING_FLAG = this.examineSpecialcard_King(bank_slot);

            let playersum = active_slot.fight();
            let banksum = bank_slot.fight();
        
            // 1. Player has an As and bank has normal cards
            if(ACTIVE_AS_FLAG && !BANK_AS_FLAG && !BANK_KING_FLAG){
                console.log(`PLAYER-SLOT : [ ${active_slot.printSlot_Console()}]`);
                console.log(`BANK-SLOT : [ ${bank_slot.printSlot_Console()}]`);
                console.log(`Bank wins [As] ${playersum} : ${banksum}`);
                console.log('');
                this.bankpoints += 1;

                if(this.bank_bet_flag == b){
                    console.log(`[!] Bet lost : -50% JETONS`);
                    this.jetons *= 0.5;
                }
                return;
        
            // 2. Player has an As and bank has a king
            } else if(ACTIVE_AS_FLAG && BANK_KING_FLAG){
                console.log(`PLAYER-SLOT : [ ${active_slot.printSlot_Console()}]`);
                console.log(`BANK-SLOT : [ ${bank_slot.printSlot_Console()}]`);
                console.log(`Player wins [As] ${playersum} : ${banksum} [K]`);
                console.log('');
                this.playerpoints += 1;

                if(this.player_bet_flag == n){
                    console.log(`[!] Bet won : +50% JETONS`);
                    this.jetons *= 1.5;
                }
                return;
        
            // 3. Player has a king and bank has normal cards
            } else if(ACTIVE_KING_FLAG && !BANK_AS_FLAG && !BANK_KING_FLAG){
                console.log(`PLAYER-SLOT : [ ${active_slot.printSlot_Console()}]`);
                console.log(`BANK-SLOT : [ ${bank_slot.printSlot_Console()}]`);
                console.log(`Player wins [K] ${playersum} : ${banksum}`);
                console.log('');
                this.playerpoints += 1;

                if(this.player_bet_flag == n){
                    console.log(`[!] Bet won : +50% JETONS`);
                    this.jetons *= 1.5;
                }
                return;
            
            // 4. Player has a king and bank has an As
            } else if(ACTIVE_KING_FLAG && BANK_AS_FLAG && !BANK_KING_FLAG){
                console.log(`PLAYER-SLOT : [ ${active_slot.printSlot_Console()}]`);
                console.log(`BANK-SLOT : [ ${bank_slot.printSlot_Console()}]`);
                console.log(`Bank wins [K] ${playersum} : ${banksum} [As]`);
                console.log('');
                this.bankpoints += 1;

                if(this.bank_bet_flag == n){
                    console.log(`[!] Bet lost : -50% JETONS`);
                    this.jetons *= 0.5;
                }
                return;
            
            // 5. bank has an As and Player has normal cards
            } else if(BANK_AS_FLAG && !ACTIVE_AS_FLAG && !ACTIVE_KING_FLAG){
                console.log(`PLAYER-SLOT : [ ${active_slot.printSlot_Console()}]`);
                console.log(`BANK-SLOT : [ ${bank_slot.printSlot_Console()}]`);
                console.log(`Player wins ${playersum} : ${banksum} [As]`);
                console.log('');
                this.playerpoints += 1;

                if(this.player_bet_flag == n){
                    console.log(`[!] Bet won : +50% JETONS`);
                    this.jetons *= 1.5;
                }
                return;
        
            // 6. bank has an As and Player has a king
            } else if(BANK_AS_FLAG && ACTIVE_KING_FLAG){
                console.log(`PLAYER-SLOT : [ ${active_slot.printSlot_Console()}]`);
                console.log(`BANK-SLOT : [ ${bank_slot.printSlot_Console()}]`);
                console.log(`Bank wins [As] ${playersum} : ${banksum} [K]`);
                console.log('');
                this.bankpoints += 1;

                if(this.bank_bet_flag == n){
                    console.log(`[!] Bet lost : -50% JETONS`);
                    this.jetons *= 0.5;
                }
                return;
        
            // 7. Bank has a king and Player has normal cards
            } else if(BANK_KING_FLAG && !ACTIVE_AS_FLAG && !ACTIVE_KING_FLAG){
                console.log(`PLAYER-SLOT : [ ${active_slot.printSlot_Console()}]`);
                console.log(`BANK-SLOT : [ ${bank_slot.printSlot_Console()}]`);
                console.log(`Bank wins ${playersum} : ${banksum} [K]`);
                console.log('');
                this.bankpoints += 1;

                if(this.bank_bet_flag == n){
                    console.log(`[!] Bet lost : -50% JETONS`);
                    this.jetons *= 0.5;
                }
                return;
            
            // 8. Bank has a king and Player has an As
            } else if(BANK_KING_FLAG && ACTIVE_AS_FLAG && !ACTIVE_KING_FLAG){
                console.log(`PLAYER-SLOT : [ ${active_slot.printSlot_Console()}]`);
                console.log(`BANK-SLOT : [ ${bank_slot.printSlot_Console()}]`);
                console.log(`Player wins [K] ${playersum} : ${banksum} [As]`);
                console.log('');
                this.playerpoints += 1;

                if(this.player_bet_flag == n){
                    console.log(`[!] Bet won : +50% JETONS`);
                    this.jetons *= 1.5;
                }
                return;
            }

            // ### Sum compare ### //
            console.log('');
            console.log(`PLAYER-SLOT | ${active_slot.fight()} [ ${active_slot.printSlot_Console()}]`);
            console.log(`BANK-SLOT | ${bank_slot.fight()} [ ${bank_slot.printSlot_Console()}]`);

            if(playersum > banksum){
                console.log(`Player wins ${playersum} : ${banksum}`);
                console.log('');
                
                if(this.player_bet_flag == n){
                    console.log(`[!] Bet won : +50% JETONS`);
                    this.jetons *= 1.5;
                }

                this.playerpoints += 1;
            } else if (playersum < banksum){
                console.log(`Bank wins ${playersum} : ${banksum}`);
                console.log('');

                if(this.bank_bet_flag == n){
                    console.log(`[!] Bet lost : -50% JETONS`);
                    this.jetons *= 0.5;
                }

                this.bankpoints += 1;
            } else {
                console.log(`Patt ${playersum} : ${banksum}`);
                console.log('');
            }
        } else {
            var active_slot = this.actors[0].accessSlot(m);
            var bank_slot = this.actors[1].accessSlot(n);

            active_slot.setChoosedFlag(true);
            bank_slot.setChoosedFlag(true);

            // Joker Rule

            const ACTIVE_JOKER_FLAG = this.examineSpecialcard_Joker(active_slot);
            if(ACTIVE_JOKER_FLAG){
                console.log('JOKER: SWAP SLOTS');
                let swapslot = this.swapSlots(active_slot,bank_slot);
                active_slot = swapslot[0];
                bank_slot = swapslot[1];
            }

            // In the case the bank had also a joker before, swap back
            const BANK_JOKER_FLAG = this.examineSpecialcard_Joker(active_slot);
            if(BANK_JOKER_FLAG){
                console.log('JOKER: SWAP SLOTS');
                let swapslot = this.swapSlots(active_slot,bank_slot);
                active_slot = swapslot[0];
                bank_slot = swapslot[1];
            }

            // ### King and As Rule ### //

            const ACTIVE_AS_FLAG = this.examineSpecialcard_As(active_slot);
            const ACTIVE_KING_FLAG = this.examineSpecialcard_King(active_slot);
            const BANK_AS_FLAG = this.examineSpecialcard_As(bank_slot);
            const BANK_KING_FLAG = this.examineSpecialcard_King(bank_slot);

            let playersum = active_slot.fight();
            let banksum = bank_slot.fight();
        
            // 1. Player has an As and bank has normal cards
            if(ACTIVE_AS_FLAG && !BANK_AS_FLAG && !BANK_KING_FLAG){
                console.log(`BANK-SLOT : [ ${bank_slot.printSlot_Console()}]`);
                console.log(`PLAYER-SLOT : [ ${active_slot.printSlot_Console()}]`);
                console.log(`Bank wins [As] ${playersum} : ${banksum}`);
                console.log('');
                this.bankpoints += 1;

                if(this.bank_bet_flag == n){
                    console.log(`[!] Bet lost : -50% JETONS`);
                    this.jetons *= 0.5;
                }
                return;
        
            // 2. Player has an As and bank has a king
            } else if(ACTIVE_AS_FLAG && BANK_KING_FLAG){
                console.log(`BANK-SLOT : [ ${bank_slot.printSlot_Console()} `);
                console.log(`PLAYER-SLOT : [ ${active_slot.printSlot_Console()}]`);
                console.log(`Player wins [As] ${playersum} : ${banksum} [K]`);
                console.log('');
                this.playerpoints += 1;

                if(this.player_bet_flag == n){
                    console.log(`[!] Bet won : +50% JETONS`);
                    this.jetons *= 1.5;
                }
                return;
        
            // 3. Player has a king and bank has normal cards
            } else if(ACTIVE_KING_FLAG && !BANK_AS_FLAG && !BANK_KING_FLAG){
                console.log(`BANK-SLOT : [ ${bank_slot.printSlot_Console()}]`);
                console.log(`PLAYER-SLOT : [ ${active_slot.printSlot_Console()}]`);
                console.log(`Player wins [K] ${playersum} : ${banksum}`);
                console.log('');
                this.playerpoints += 1;

                if(this.player_bet_flag == n){
                    console.log(`[!] Bet won : +50% JETONS`);
                    this.jetons *= 1.5;
                }
                return;
            
            // 4. Player has a king and bank has an As
            } else if(ACTIVE_KING_FLAG && BANK_AS_FLAG && !BANK_KING_FLAG){
                console.log(`BANK-SLOT : [ ${bank_slot.printSlot_Console()}]`);
                console.log(`PLAYER-SLOT : [ ${active_slot.printSlot_Console()}]`);
                console.log(`Bank wins [K] ${playersum} : ${banksum} [As]`);
                console.log('');
                this.bankpoints += 1;

                if(this.bank_bet_flag == n){
                    console.log(`[!] Bet lost : -50% JETONS`);
                    this.jetons *= 0.5;
                }
                return;
            
            // 5. Bank has an As and Player has normal cards
            } else if(BANK_AS_FLAG && !ACTIVE_AS_FLAG && !ACTIVE_KING_FLAG){
                console.log(`BANK-SLOT : [ ${bank_slot.printSlot_Console()}]`);
                console.log(`PLAYER-SLOT : [ ${active_slot.printSlot_Console()}]`);
                console.log(`Player wins ${playersum} : ${banksum} [As]`);
                console.log('');
                this.playerpoints += 1;

                if(this.player_bet_flag == n){
                    console.log(`[!] Bet won : +50% JETONS`);
                    this.jetons *= 1.5;
                }
                return;
        
            // 6. Bank has an As and Player has a king
            } else if(BANK_AS_FLAG && ACTIVE_KING_FLAG){
                console.log(`BANK-SLOT : [ ${bank_slot.printSlot_Console()}]`);
                console.log(`PLAYER-SLOT : [ ${active_slot.printSlot_Console()}]`);
                console.log(`Bank wins [As] ${playersum} : ${banksum} [K]`);
                console.log('');
                this.bankpoints += 1;

                if(this.bank_bet_flag == n){
                    console.log(`[!] Bet lost : -50% JETONS`);
                    this.jetons *= 0.5;
                }
                return;
        
            // 7. Bank has a king and Player has normal cards
            } else if(BANK_KING_FLAG && !ACTIVE_AS_FLAG && !ACTIVE_KING_FLAG){
                console.log(`BANK-SLOT : [ ${bank_slot.printSlot_Console()}]`);
                console.log(`PLAYER-SLOT : [ ${active_slot.printSlot_Console()}]`);
                console.log(`Bank wins ${playersum} : ${banksum} [K]`);
                console.log('');
                this.bankpoints += 1;

                if(this.bank_bet_flag == n){
                    console.log(`[!] Bet lost : -50% JETONS`);
                    this.jetons *= 0.5;
                }
                return;
            
            // 8. Bank has a king and Player has an As
            } else if(BANK_KING_FLAG && ACTIVE_AS_FLAG && !ACTIVE_KING_FLAG){
                console.log(`BANK-SLOT : [ ${bank_slot.printSlot_Console()}]`);
                console.log(`PLAYER-SLOT : [ ${active_slot.printSlot_Console()}]`);
                console.log(`Player wins [K] ${playersum} : ${banksum} [As]`);
                console.log('');
                this.playerpoints += 1;

                if(this.player_bet_flag == n){
                    console.log(`[!] Bet won : +50% JETONS`);
                    this.jetons *= 1.5;
                }
                return;
            }

            // ### Sum compare ### //
            console.log('');
            console.log(`BANK-SLOT | ${bank_slot.fight()} [ ${bank_slot.printSlot_Console()}]`);
            console.log(`PLAYER-SLOT | ${active_slot.fight()} [ ${active_slot.printSlot_Console()}]`);

            if(playersum > banksum){
                console.log(`Player wins ${playersum} : ${banksum}`);
                console.log('');

                if(this.player_bet_flag == n){
                    console.log(`[!] Bet won : +50% JETONS`);
                    this.jetons *= 1.5;
                }

                this.playerpoints += 1;
            } else if (playersum < banksum){
                console.log(`Bank wins ${playersum} : ${banksum}`);
                console.log('');

                if(this.bank_bet_flag == n){
                    console.log(`[!] Bet lost : -50% JETONS`);
                    this.jetons *= 0.5;
                }

                this.bankpoints += 1;
            } else {
                console.log(`Patt ${playersum} : ${banksum}`);
                console.log('');
            }
        }
    }

    /**
     * tests if this slot contains a joker
     * @param {Cardslot} slot active cardslot 
     * @returns true if there is a joker
     */
    examineSpecialcard_Joker(slot){
        const active_slot = slot.accessSlotcards();
        for(let n = 0; n < active_slot.length; n++){
            if(active_slot[n].isSpecialcard()[0]){
                return true;
            }
        }
        return false;
    }

    /**
     * swaps two slots
     * @param {Cardslot} active active cardslot 
     * @param {Cardslot} bank bank cards lot
     * @returns array of the swapped slots
     */
    swapSlots(active, bank){
        return [bank, active];
    }

    /**
     * tests if this slot contains an As
     * @param {Cardslot} slot active cardslot 
     * @returns true if there is an As
     */
    examineSpecialcard_As(slot){
        const active_slot = slot.accessSlotcards();
        for(let n = 0; n < active_slot.length; n++){
            if(active_slot[n].isSpecialcard()[1]){
                return true;
            }
        }
        return false;  
    }

    /**
     * tests if this slot contains a king
     * @param {Cardslot} slot active cardslot 
     * @returns true if there is a king
     */
    examineSpecialcard_King(slot){
        const active_slot = slot.accessSlotcards();
        for(let n = 0; n < active_slot.length; n++){
            if(active_slot[n].isSpecialcard()[2]){
                return true;
            }
        }
        return false;  
    }

    /**
     * shows the status after one round in a campagne
     * @returns true, the round continues
     */
    showResults(){
        console.log(`Player ${this.playerpoints} : ${this.bankpoints} Bank`);

        if(this.playerpoints == 3 && this.bankpoints <= 2){
            console.log(`Player wins`);
            this.continueCampagne = true;
            return true;
        } else if(this.playerpoints <= 2 && this.bankpoints == 3){
            console.log(`Bank wins`);
            this.continueCampagne = false;
            return true;
        } else if(this.playerpoints == 3 && this.patts == 1){
            console.log(`Player wins`);
            this.continueCampagne = true;
            return true;
        } else if(this.bankpoints == 3 && this.patts == 1){
            console.log(`Bank wins`);
            this.continueCampagne = false;
            return true;
        } else if(this.playerpoints == this.bankpoints && this.patts == 1){
            console.log(`Patt`);
            this.continueCampagne = false;
            return true;
        } else if(this.playerpoints == 2 && this.bankpoints == 1 && this.patts == 2){
            console.log(`Player wins`);
            this.continueCampagne = true;
            return true;
        } else if(this.playerpoints == 1 && this.bankpoints == 2 && this.patts == 2){
            console.log(`Bank wins`);
            this.continueCampagne = false;
            return true;
        } else if(this.playerpoints == 2 && this.patts == 3){
            console.log(`Player wins`);
            this.continueCampagne = true;
            return true;
        } else if(this.bankpoints == 2 && this.patts == 3){
            console.log(`Bank wins`);
            this.continueCampagne = false;
            return true;
        } else if(this.playerpoints == this.bankpoints && this.patts == 3){
            console.log(`Patt`);
            this.continueCampagne = false;
            return true;
        } else if(this.playerpoints == 1 && this.patts == 4){
            console.log(`Player wins`);
            this.continueCampagne = true;
            return true;
        } else if(this.bankpoints == 1 && this.patts == 4){
            console.log(`Bank wins`);
            this.continueCampagne = false;
            return true;
        } else if(this.patts == 5){
            console.log(`Patt`);
            this.continueCampagne = false;
            return true;
        } else {
            return false;
        }
    }

    /**
     * examines if the slots are placed with cards or not
     * @param {int} actorindex 0 = player, 1 = bank
     * @returns if the slot is valid
     */
    slotsAreValid(actorindex){
        const ACTOR = this.actors[actorindex];
        for(let n = 0; n < 5; n++){
            if(ACTOR.accessSlot(n).slot.length == 0){
                return false;
            }
        }
        return true;
    }

    /**
     * examines if all cards into this slot are valid
     * @param {int} actorindex actorindex 
     * @param {int} slotindex slotindex
     * @returns slotcards are valid
     */
    slotcontentIsValid(actorindex, slotindex){
        const SLOT = this.actors[actorindex].accessSlot(slotindex).accessSlotcards();

        if(SLOT.length == 1 && SLOT[0].isSpecialcard()[0]){
            console.log('[!] Joker is not allowed to be alone in a slot');
            return false;
        } else if(SLOT.length > 1){
            let as_flag = false;
            let as_counter = 0;
            let king_flag = false;
            let king_counter = 0;
            SLOT.forEach(card => {
                if(card.isSpecialcard()[1]){
                    as_flag = true;
                    as_counter += 1;
                }
            });

            SLOT.forEach(card => {
                if(card.isSpecialcard()[2]){
                    king_flag = true;
                    king_counter += 1;
                }
            });

            if(as_flag && king_flag){
                console.log('[!] As and King in the same Slot are not valid');
                return false;
            }

            if(as_counter > 1 || king_counter > 1){
                console.log('[!] Only one as or one king in the same slot allowed');
                return false;
            }
        }
        return true;

    }

}

module.exports = { Observer : Observer };