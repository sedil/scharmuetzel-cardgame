const {Cardslot} = require('./Cardslot');

/**
 * This class represents an actor
 */
class Actor {

    constructor(){
        this.actorcards = new Array();
        this.slots = [new Cardslot(),new Cardslot(),new Cardslot(),new Cardslot(),new Cardslot()];
    }

    /**
     * take a card from the stack
     * @param {int} index index of the actor's cardstack
     * @returns a card
     */
    takeCard(index){
        if(this.actorcards.length > 0){
            return this.actorcards.splice(index, 1)[0];
        } else {
            console.warn('No more cards in the stack');
        }
    }

    /**
     * pushes a card back into the cardstack
     * @param {Card} card card
     */
    pushCard(card){
        this.actorcards.push(card);
    }

    /**
     * accesses the slot with the index as argument
     * @param {int} slotindex index of the slot 
     * @returns slotaccess
     */
    accessSlot(slotindex){
        return this.slots[slotindex];
    }

    /**
     * returns the actor cards
     * @returns actorcards
     */
    getActorcards(){
        return this.actorcards;
    }
}

module.exports = { Actor : Actor };