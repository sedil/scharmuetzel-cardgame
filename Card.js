const {Cardimage} = require("./Cardstat");

/**
 * This class creates cards for the game
 */
class Card {

    constructor(value){
        this.cardvalue = value;
        this.cardimage = value;
        this.joker = false;
        this.as = false;
        this.king = false;
        if(value == 0){
            this.joker = true;
            this.cardimage = Cardimage.JOKER;
        } else if (value == 1){
            this.as = true;
            this.cardimage = Cardimage.ASS;
        } else if(value == 11){
            this.cardvalue = 10;
            this.cardimage = Cardimage.JACK;
        } else if(value == 12){
            this.cardvalue = 10;
            this.cardimage = Cardimage.QUEEN;
        } else if(value == 13){
            this.king = true;
            this.cardvalue = 10;
            this.cardimage = Cardimage.KING;
        }
    }

    /**
     * returns the cardvalue
     * @returns cardvalue
     */
    getValue(){
        return this.cardvalue;
    }

    /**
     * return the card's image
     * @returns cardimage
     */
    getImage(){
        return this.cardimage;
    }

    /**
     * returns the flagarray if this card is a special card
     * [0] = joker
     * [1] = as
     * [2] = king
     * @returns flagarray of this card
     */
    isSpecialcard(){
        return [this.joker,this.as,this.king];
    }

    toString(){
        return `${this.cardvalue} ${this.cardimage} ${this.isSpecialcard()}`;
    }
}

module.exports = { Card : Card };