/**
 * Represents a Cardslot which contains cards
 */
class Cardslot {

    constructor(){
        this.slot = Array();
        this.choosed = false;
    }

    /**
     * inserts a card into this slot
     * @param {Card} card card
     */
    insertCardIntoSlot(card){
        if(this.slot.length < 4){
            this.slot.push(card);
        } else {
            console.warn('Slot is full');
        }
    }

    /**
     * the sum of the cardvalues
     * @returns sum of all cards in this slot
     */
    fight(){
        let cardsum = 0;
        this.slot.forEach(card => {
            cardsum += card.getValue();
        });
        return cardsum;
    }
    
    /**
     * prints all cardimages from this slot
     * @returns string of cardimages
     */
    printSlot_Console(){
        let images = '';
        this.slot.forEach(card => images += card.getImage() + ' ');
        return images;
    }

    /**
     * sets the flag if this slot was choosed
     * @param {boolean} choose is choosed
     */
    setChoosedFlag(choose){
        this.choosed = choose;
    }

    /**
     * returns if this slot was already choosed
     * @returns was choosed
     */
    IsChoosed(){
        return this.choosed;
    }

    /**
     * returns all cards
     * @returns all cards from this slot
     */
    getSlotcards(){
        return this.slot.splice(0, this.slot.length);
    }

    /**
     * removes the last card from this slot e.g. if a card was inserted invalid
     * @returns last card
     */
    getLastSlotcard(){
        return this.slot.pop();
    }

    /**
     * accesses the cardstack in this slot
     * @returns slotcards
     */
    accessSlotcards(){
        return this.slot;
    }
}

module.exports = { Cardslot : Cardslot};